package xyz.sebastienm4j.midi.support.web

import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import org.springframework.web.context.WebApplicationContext

@Component
class WikiHolder(
    val currentWikiHolder: CurrentWikiHolder,
    val defaultWikiHolder: DefaultWikiHolder
) {
    fun namespace() = currentWikiHolder?.namespace ?: defaultWikiHolder.namespace

    fun wiki() = currentWikiHolder?.wiki ?: defaultWikiHolder.wiki

    fun branch() = currentWikiHolder?.branch ?: defaultWikiHolder.branch
}

@Component
@Scope(
    value = WebApplicationContext.SCOPE_SESSION,
    proxyMode = ScopedProxyMode.TARGET_CLASS
)
data class CurrentWikiHolder(
    var namespace: String? = null,
    var wiki: String? = null,
    var branch: String? = null
)

@Component
data class DefaultWikiHolder(
    var namespace: String = "",
    var wiki: String = "",
    var branch: String = ""
)