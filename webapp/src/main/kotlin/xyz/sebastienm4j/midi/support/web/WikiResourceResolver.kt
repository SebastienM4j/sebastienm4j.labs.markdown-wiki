package xyz.sebastienm4j.midi.support.web

import mu.KotlinLogging
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component
import org.springframework.web.servlet.resource.AbstractResourceResolver
import org.springframework.web.servlet.resource.ResourceResolverChain
import javax.servlet.http.HttpServletRequest
import xyz.sebastienm4j.midi.support.web.logger as klogger

private val logger = KotlinLogging.logger {}

@Component
class WikiResourceResolver(
        val wikiHolder: WikiHolder
) : AbstractResourceResolver()
{
    override fun resolveResourceInternal(request: HttpServletRequest?, requestPath: String, locations: MutableList<out Resource>, chain: ResourceResolverChain): Resource?
    {
        val wikiRequestPath = "${wikiHolder.namespace()}/${wikiHolder.wiki()}/${wikiHolder.branch()}/$requestPath"

        klogger.debug("Resolve resource for request path [{}] as [{}]", requestPath, wikiRequestPath)

        val resource = chain.resolveResource(request, wikiRequestPath, locations)
        if(resource == null || request == null) {
            return null
        }

        return resource
    }

    override fun resolveUrlPathInternal(resourceUrlPath: String, locations: MutableList<out Resource>, chain: ResourceResolverChain): String?
    {
        klogger.debug("Resolve URL for resource URL path [{}]", resourceUrlPath)

        return chain.resolveUrlPath(resourceUrlPath, locations)
    }



}