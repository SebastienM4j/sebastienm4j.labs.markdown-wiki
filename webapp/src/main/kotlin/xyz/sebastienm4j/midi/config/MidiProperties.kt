package xyz.sebastienm4j.midi.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Configuration

@Configuration
@ConstructorBinding
@ConfigurationProperties(prefix = "midi")
class MidiProperties(
    var wikis: Set<WikiProperties> = emptySet(),
    var sourceBaseDir: String = "",
    var wikiBaseDir: String = "",
    var nodeJsHome: String? = null
)
{
    data class WikiProperties(
        val name: String,
        val gitURL: String,
        val gitUsername: String,
        val gitPassword: String
    )
}