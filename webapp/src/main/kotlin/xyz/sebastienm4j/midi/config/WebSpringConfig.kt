package xyz.sebastienm4j.midi.config

import mu.KotlinLogging
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import xyz.sebastienm4j.midi.support.web.WikiResourceResolver
import java.nio.file.Paths


private val logger = KotlinLogging.logger {}

@Configuration
@EnableWebMvc
class WebSpringConfig(
        val midiProperties: MidiProperties,
        val wikiResourceResolver: WikiResourceResolver
) : WebMvcConfigurer
{
    override fun addResourceHandlers(registry: ResourceHandlerRegistry)
    {
        val wikiBaseDirPath = Paths.get(midiProperties.wikiBaseDir).toAbsolutePath().toString()

        logger.debug("Serves wiki resources from [{}/]", wikiBaseDirPath)

        registry.addResourceHandler("/**")
                .addResourceLocations("file:$wikiBaseDirPath/")
                .resourceChain(true)
                .addResolver(wikiResourceResolver)
    }

    override fun addViewControllers(registry: ViewControllerRegistry) {
        registry.addRedirectViewController("/", "index.html")
    }
}