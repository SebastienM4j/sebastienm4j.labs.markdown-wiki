package xyz.sebastienm4j.midi.config

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
@Profile("disable-security")
class DisableSecuritySpringConfig : WebSecurityConfigurerAdapter()
{
    override fun configure(http: HttpSecurity?)
    {
        http!!.authorizeRequests()
                .anyRequest().permitAll()
    }
}
