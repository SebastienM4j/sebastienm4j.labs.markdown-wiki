package xyz.sebastienm4j.midi.domain.wiki

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import xyz.sebastienm4j.midi.config.MidiProperties
import java.nio.file.Paths

private const val PUBLIC_NAMESPACE = "public"

class PublicWiki(
    wikiName: String,
    midiProperties: MidiProperties,
    yamlMapper: ObjectMapper
) : Wiki(wikiName, midiProperties, yamlMapper)
{
    override fun repoDirPath() = Paths.get(midiProperties.sourceBaseDir, PUBLIC_NAMESPACE, wikiName)

    override fun repoGitDirPath() = Paths.get(repoDirPath().toAbsolutePath().toString(), ".git")

    override fun vuepressDirPath() = Paths.get(repoDirPath().toAbsolutePath().toString(), config?.dir ?: ".")

    override fun vuepressDistDirPath() = Paths.get(vuepressDirPath().toAbsolutePath().toString(), "src", ".vuepress", "dist")

    override fun wikiDestDirPath() = Paths.get(midiProperties.wikiBaseDir, PUBLIC_NAMESPACE, wikiName, git?.repository?.branch)
}

@Configuration
class PublicWikiFactorySpringConfig(
    val midiProperties: MidiProperties,
    val yamlMapper: ObjectMapper
) {
    @Bean
    fun publicWikiFactory() : (wikiName: String) -> PublicWiki = this::publicWiki

    @Bean
    @Scope("prototype")
    fun publicWiki(wikiName: String) : PublicWiki
    {
        return PublicWiki(wikiName, midiProperties, yamlMapper)
    }
}