package xyz.sebastienm4j.midi.domain.bootstrap

class BootstrapException : RuntimeException
{
    constructor(message: String?) : super(message)
}