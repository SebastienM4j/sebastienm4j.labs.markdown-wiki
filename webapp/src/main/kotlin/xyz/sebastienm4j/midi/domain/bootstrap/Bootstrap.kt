package xyz.sebastienm4j.midi.domain.bootstrap

import mu.KotlinLogging
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import xyz.sebastienm4j.midi.config.MidiProperties
import xyz.sebastienm4j.midi.domain.wiki.PublicWiki
import xyz.sebastienm4j.midi.support.web.DefaultWikiHolder
import java.nio.file.Paths

private val logger = KotlinLogging.logger {}

private const val PUBLIC_NAMESPACE = "public"

@Component
class Bootstrap(
    val midiProperties: MidiProperties,
    val publicWikiFactory: (wikiName: String) -> PublicWiki,
    val defaultWikiHolder: DefaultWikiHolder
) {
    @EventListener
    fun bootstrap(event: ContextRefreshedEvent)
    {
        checkDirExistsOrCreateIt("sourceBaseDir", midiProperties.sourceBaseDir)
        checkDirExistsOrCreateIt("wikiBaseDir", midiProperties.wikiBaseDir)

        initializeWikis()
    }


    private fun checkDirExistsOrCreateIt(propertyName: String, dir: String)
    {
        val dirPath = Paths.get(dir)
        val dirFile = dirPath.toFile()

        if(dirFile.exists() && dirFile.isDirectory && dirFile.canRead() && dirFile.canWrite()) {
            logger.debug("{} [{}] exists, nothing to do", propertyName, dir)
        } else if(dirFile.exists()) {
            throw BootstrapException("$propertyName [$dir] exists but is not a directory or there have problem with permissions")
        } else if(dirFile.mkdirs()) {
            logger.info("{} [{}] have been created", propertyName, dir)
        } else {
            throw BootstrapException("$propertyName [$dir] cannot be created")
        }
    }

    private fun initializeWikis()
    {
        val wikis = midiProperties.wikis.map { publicWikiFactory.invoke(it.name) }

        wikis.forEach(PublicWiki::initialize)

        defaultWikiHolder.namespace = PUBLIC_NAMESPACE
        defaultWikiHolder.wiki = wikis[0].wikiName
        defaultWikiHolder.branch = wikis[0].branch() ?: "master"
    }
}