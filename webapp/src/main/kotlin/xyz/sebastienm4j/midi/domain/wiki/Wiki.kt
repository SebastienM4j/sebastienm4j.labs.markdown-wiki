package xyz.sebastienm4j.midi.domain.wiki

import com.fasterxml.jackson.databind.ObjectMapper
import mu.KotlinLogging
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import xyz.sebastienm4j.midi.config.MidiProperties
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.TimeUnit

private val logger = KotlinLogging.logger {}

abstract class Wiki(
    val wikiName: String,
    val midiProperties: MidiProperties,
    val yamlMapper: ObjectMapper
) {
    val properties = midiProperties.wikis.filter { it.name == wikiName }.first()

    val credentials = UsernamePasswordCredentialsProvider(properties.gitUsername, properties.gitPassword)

    var git: Git? = null

    var config: WikiConfig? = null


    abstract fun repoDirPath() : Path

    abstract fun repoGitDirPath() : Path

    abstract fun vuepressDirPath() : Path

    abstract fun vuepressDistDirPath() : Path

    abstract fun wikiDestDirPath() : Path


    fun isCloned() = repoDirPath().toFile().exists()

    fun branch() = git?.repository?.branch


    fun initialize()
    {
        if(!isCloned()) {
            clone()
        } else {
            pull()
        }

        loadConfig()

        buildVuepress()

        deploy()

        logger.info("Wiki [{}] is initialized", wikiName)
    }

    fun clone()
    {
        if(isCloned()) {
            return
        }

        logger.info("Clone repository of wiki [{}] from url [{}] with username [{}]", wikiName, properties.gitURL, properties.gitUsername)

        git = Git.cloneRepository()
                 .setURI(properties.gitURL)
                 .setDirectory(repoDirPath().toFile())
                 .setCredentialsProvider(credentials)
                 .call()
    }

    fun pull()
    {
        if(!isCloned()) {
            throw IllegalStateException("The repository of wiki $wikiName must be cloned before")
        }

        logger.info("Pull repository of wiki [{}] from url [{}] with username [{}]", wikiName, properties.gitURL, properties.gitUsername)

        if(git == null) {
            git = Git.open(Paths.get(repoDirPath().toAbsolutePath().toString(), ".git").toFile())
        }

        git?.pull()
           ?.setRebase(true)
           ?.setCredentialsProvider(credentials)
           ?.call()
    }

    fun loadConfig()
    {
        config = Files.newBufferedReader(Paths.get(repoDirPath().toString(), "midi.yml")).use {
            yamlMapper.readValue(it, WikiConfig::class.java)
        }
    }

    fun buildVuepress()
    {
        logger.info("Build wiki [{}] in dir [{}]", wikiName, vuepressDirPath().toAbsolutePath().toString())

        val builder = ProcessBuilder()
        builder.directory(vuepressDirPath().toFile())

        midiProperties.nodeJsHome?.let {
            val nodeJsHome = Paths.get(it, "bin").normalize().toAbsolutePath().toString()
            builder.environment()["PATH"] = builder.environment()["PATH"]+":"+nodeJsHome
        }
        if(logger.isDebugEnabled) {
            builder.environment().entries.forEach { env ->
                logger.debug("Environnement variable for building wiki [{}] = [{}]", env.key, env.value)
            }
        }

        builder.command("sh", "-c", "npm install ; npm run build")
        builder.redirectInput()
        builder.redirectError()

        val process = builder.start()
        process.waitFor(3, TimeUnit.MINUTES)

        if(process.exitValue() == 0) {
            logger.info("Build wiki [{}] finished with success", wikiName)
            process.inputStream.reader(Charsets.UTF_8).use {
                logger.debug("Build wiki [{}] output : [{}]", wikiName, it.readText())
            }

        } else {
            logger.error("Build wiki [{}] fails with exit value [{}]", wikiName, process.exitValue())
            process.inputStream.reader(Charsets.UTF_8).use {
                logger.error("Build wiki [{}] output : [{}]", wikiName, it.readText())
            }
        }
    }

    fun deploy()
    {
        val vuepressDistDir = vuepressDistDirPath()
        val wikiDestDir = wikiDestDirPath()

        logger.info("Deploy wiki [{}], copy files from [{}] into [{}]", wikiName, vuepressDistDir.toAbsolutePath().toString(), wikiDestDir.toAbsolutePath().toString())

        vuepressDistDir.toFile().copyRecursively(wikiDestDir.toFile(), true)
    }
}